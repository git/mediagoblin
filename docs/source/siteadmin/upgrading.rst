.. MediaGoblin Documentation

   Written in 2020 by MediaGoblin contributors

   To the extent possible under law, the author(s) have dedicated all
   copyright and related and neighboring rights to this software to
   the public domain worldwide. This software is distributed without
   any warranty.

   You should have received a copy of the CC0 Public Domain
   Dedication along with this software. If not, see
   <http://creativecommons.org/publicdomain/zero/1.0/>.

======================
 Upgrading MediaGoblin
======================

Preparation
-----------

*ALWAYS* take a backup before upgrading, especially before running migrations. That
way if something goes wrong, we can fix things.

Although not strictly necessary, we recommend you shut down your current
MediaGoblin/Celery processes before upgrading.

If you have any problems, please drop in to the `#mediagoblin IRC chat
<https://web.libera.chat/#mediagoblin>`_, report an issue on our `issue
tracker <https://todo.sr.ht/~mediagoblin/mediagoblin>`_ or drop us an email to
`mediagoblin-devel@gnu.org <mailto:mediagoblin-devel@gnu.org>`_.

.. note::

   This page describes upgrading of our traditional virtualenv installation
   method. If Docker is your thing, please see ":doc:`docker`".


Upgrade
-------

1. Switch to the user you used to deploy MediaGoblin, which may be "mediagoblin"
   if you followed the deployment guide::

     sudo su mediagoblin --shell=/bin/bash

2. Update to the latest release.  In your ``mediagoblin`` directory, run::

     git fetch && git checkout --quiet v0.14.0 && git submodule update

   If you are checking out a branch, rather than a specific version tag, please
   run `git pull` before `git submodule update`.

3. Note down any plugins you have installed by reviewing your
   ``mediagoblin.ini`` configuration. These will be removed by the following
   steps and must be re-installed.

4. Remove your existing installation::

     make distclean

5. Recreate the virtual environment and install MediaGoblin::

     ./autogen.sh && ./configure && make

   In case of errors, you may need to double-check that the relevant
   dependencies are installed and or correct the file permissions as mentioned
   in ":doc:`deploying`".

6. Re-install any ":doc:`plugins`" you had previously installed. Skipping these
   may result in errors updating the database.

7. Update the database::

     ./bin/gmg dbupdate

8. Restart the Paster and Celery processes. If you followed ":doc:`deploying`",
   this may be something like::

     # Switch back to your original user with Ctrl-d or "exit".
     sudo systemctl restart mediagoblin-paster.service
     sudo systemctl restart mediagoblin-celeryd.service

   To see the logs for troubleshooting, use something like::

     sudo journalctl --unit mediagoblin-paster.service --follow
     sudo journalctl --unit mediagoblin-celeryd.service --follow

9. View your site and hover your cursor over the "MediaGoblin" link in the
   footer to confirm the version number you're running.


Updating your system Python
---------------------------

Upgrading your operating system or installing a new major version of Python may
break MediaGoblin. This typically occurs because Python virtual environment is
referring to a copy of Python that no longer exists. In this situation use the
same process for "Upgrade" above.

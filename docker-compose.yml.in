
version: '3'

services:
  paste:
    image: mediagoblin/mediagoblin:@PACKAGE_VERSION@
    # build:
    #   context: .
    #   dockerfile: Dockerfile-debian-12-sqlite
    #   args:
    #     build_doc: no
    #     run_tests: no
    depends_on:
      rabbitmq:
        condition: service_started
    volumes:
      - mediagoblin-data:/srv:rw
    ports:
      - "6543:6543"
    env_file: docker-compose.env
    environment:
      - 'CELERY_ALWAYS_EAGER=false'
      - 'BROKER_URL=amqp://rabbitmq:5672'

    # XXX need host = 0.0.0.0 for server:main, or a way to select
    # server-name=broadcast
    # command: /opt/mediagoblin/bin/gmg -cf /srv/mediagoblin.ini serve /srv/paste.ini
    command: /opt/mediagoblin/bin/paster serve /srv/paste.ini --server-name=broadcast
    healthcheck:
      test: [ "CMD", "curl", "-sf", "http://localhost:6543" ]

  celery:
    image: mediagoblin/mediagoblin:@PACKAGE_VERSION@
    depends_on:
      rabbitmq:
        condition: service_healthy
      paste:
        condition: service_started
    volumes:
      - mediagoblin-data:/srv:rw
    env_file: docker-compose.env
    environment:
      - 'CELERY_CONFIG_MODULE=mediagoblin.init.celery.from_celery'
      - 'MEDIAGOBLIN_CONFIG=/srv/mediagoblin.ini'
      - 'SKIP_MIGRATE=true'
      - 'BROKER_URL=amqp://rabbitmq:5672'
    # command: /opt/mediagoblin/bin/gmg -cf /srv/mediagoblin.ini celery
    command: /opt/mediagoblin/bin/celery worker
    healthcheck:
      test: [ "CMD", "/opt/mediagoblin/bin/celery", "inspect", "ping" ]

  rabbitmq:
    image: rabbitmq
    expose:
      - "5672"
    healthcheck:
      test: [ "CMD", "rabbitmq-diagnostics", "-q", "ping" ]

volumes:
  mediagoblin-data:
    driver_opts:
      # Present the local ./data directory as a named volume
      type: none
      o: bind
      device: ${PWD}/data

PACKAGE_BUGREPORT = @PACKAGE_BUGREPORT@
PACKAGE_NAME = @PACKAGE_NAME@
PACKAGE_STRING = @PACKAGE_STRING@
PACKAGE_TARNAME = @PACKAGE_TARNAME@
PACKAGE_URL = @PACKAGE_URL@
PACKAGE_VERSION = @PACKAGE_VERSION@
PACKAGE_DISTNAME = ${PACKAGE_NAME}-${PACKAGE_VERSION}
PACKAGE_DOCKER_REPO = mediagoblin
PACKAGE_DOCKER_IMAGE = $(PACKAGE_DOCKER_REPO)/${PACKAGE_NAME}:${PACKAGE_VERSION}
NGINX_DISTNAME = nginx-${PACKAGE_VERSION}
NGINX_DOCKER_IMAGE = $(PACKAGE_DOCKER_REPO)/nginx:${PACKAGE_VERSION}

DOCKER=@DOCKER@

PYTHON=@PYTHON@
PYTHON_VERSION=@PYTHON_VERSION@

NPX=@NPX@
BOWER_ARGS=

BUILD_AUDIO=@BUILD_AUDIO@
BUILD_VIDEO=@BUILD_VIDEO@
BUILD_RAW_IMAGE=@BUILD_RAW_IMAGE@
BUILD_PDF=@BUILD_PDF@
BUILD_STL=@BUILD_STL@
BUILD_LDAP=@BUILD_LDAP@
BUILD_DOC=no

VENV_SUBDIR=@VENV_SUBDIR@

# XXX: If we ever want to support out-of-source builds,
# we'll have to check that those SRC/BUILD _DIR variables are used correctly
SRC_DIR=$(CURDIR)
BUILD_DIR=$(CURDIR)
EXTLIB_DIR=$(SRC_DIR)/extlib
I18N_DIR=$(SRC_DIR)/mediagoblin/i18n
DOCS_BUILD_DIR=$(SRC_DIR)/docs/build

DOCKER_PATH=/opt/mediagoblin

EXTLIB_JS=\
	 $(EXTLIB_DIR)/jquery \
	 $(EXTLIB_DIR)/leaflet \
	 $(EXTLIB_DIR)/mediagoblin-extlib \
	 $(EXTLIB_DIR)/video.js \
	 $(EXTLIB_DIR)/videojs-resolution-switcher \
	 # end

VENV_DIR=$(BUILD_DIR)/$(VENV_SUBDIR)
VENV_PYTHON=$(VENV_DIR)/bin/python
VENV_PIP=$(VENV_DIR)/bin/pip
VENV_PYBABEL=$(VENV_DIR)/bin/pybabel
VENV_PYTEST=$(VENV_PYTHON) -m pytest
VENV_SPHINX_BUILD=$(VENV_PYTHON) -m sphinx
VENV_EDITABLE=
VENV_DIST_INFO=$(VENV_DIR)/lib/python$(PYTHON_VERSION)/site-packages/$(PACKAGE_DISTNAME).dist-info/
EGG_INFO=$(BUILD_DIR)/mediagoblin.egg-info

PACKAGE_WHEEL=dist/$(PACKAGE_DISTNAME)0-py3-none-any.whl

all: build check
dist: $(PACKAGE_WHEEL)

ifeq ($(DOCKER),none)

build: $(VENV_DIST_INFO) extlib i18n

extlib: $(EXTLIB_JS)
$(EXTLIB_JS):
	# Using `npx` rather than `npm exec` as was only added in npm v7.0.0,
	# which isn't available on Ubuntu 20.04.
	$(NPX) bower --yes $(BOWER_ARGS) install $(SRC_DIR)/bower.json

# Just depend on one random translation file to avoid rebuilding them all
# XXX: The script currently tries to find pybabel itself
# XXX: Maybe wrap the script into this Makefile instead?
i18n: $(I18N_DIR)/fr/LC_MESSAGES/mediagoblin.mo
$(I18N_DIR)/%/LC_MESSAGES/mediagoblin.mo: $(I18N_DIR)/%/mediagoblin.po $(VENV_PYBABEL)
	$(SRC_DIR)/devtools/compile_translations.sh

check: $(VENV_PIP)
	$(VENV_PIP) install $(PIP_INSTALL_ARGS) $(SRC_DIR)[test]
	$(VENV_PYTEST)

$(VENV_PYBABEL): $(VENV_DIST_INFO)
$(VENV_DIST_INFO): $(VENV_PIP) $(SRC_DIR)/setup.cfg
	$(VENV_PIP) install $(PIP_INSTALL_ARGS) $(VENV_EDITABLE) $(SRC_DIR)
	# We cannot install multiple variants of the package as editable,
	# so all extras are installed in a separate run
	$(VENV_PIP) install $(PIP_INSTALL_ARGS) \
		$(SRC_DIR)[dev] \
		$(shell test x$(BUILD_AUDIO) = xyes && echo $(SRC_DIR)[audio]) \
		$(shell test x$(BUILD_VIDEO) = xyes && echo $(SRC_DIR)[video]) \
		$(shell test x$(BUILD_RAW_IMAGE) = xyes && echo $(SRC_DIR)[raw_image]) \
		$(SRC_DIR)[ascii] \
		$(shell test x$(BUILD_LDAP) = xyes && echo $(SRC_DIR)[ldap]) \
		$(SRC_DIR)[openid] \
		# end

# XXX: add to docker
docs: $(DOCS_BUILD_DIR)
$(DOCS_BUILD_DIR): $(VENV_PIP)
	$(VENV_PIP) install $(PIP_INSTALL_ARGS) $(SRC_DIR)[doc]
	$(MAKE) -C docs html SPHINXBUILD="$(VENV_SPHINX_BUILD)"

$(VENV_PYTHON) $(VENV_PIP): $(VENV_DIR)/bin
$(VENV_DIR)/bin: # use bin to identify in-source virtualenvs
	$(PYTHON) -m venv --system-site-packages $(VENV_DIR)

$(PACKAGE_WHEEL): $(SRC_DIR)/setup.py $(VENV_DIST_INFO)
	$(VENV_PYTHON) $(SRC_DIR)/setup.py bdist_wheel


else
all: build

build: $(PACKAGE_DISTNAME).docker-stamp $(NGINX_DISTNAME).docker-stamp
$(PACKAGE_DISTNAME).docker-stamp:
	$(DOCKER) build \
		--build-arg build_doc=true \
		--build-arg build_dist=true \
		--build-arg run_tests=false \
		--tag $(PACKAGE_DOCKER_IMAGE) \
		.
	echo $(PACKAGE_DOCKER_IMAGE) > $(@)

$(NGINX_DISTNAME).docker-stamp: $(PACKAGE_DISTNAME).docker-stamp Dockerfile.nginx nginx.conf.template
	docker build -f Dockerfile.nginx . -t $(NGINX_DOCKER_IMAGE)

# make is not installed in the final container
check: $(PACKAGE_DISTNAME).docker-stamp
	$(DOCKER) run -it --rm -w $(DOCKER_PATH) \
		--entrypoint bash $(PACKAGE_DOCKER_IMAGE) -c ' \
		./$(VENV_SUBDIR)/bin/pip install $(PIP_INSTALL_ARGS) $(DOCKER_PATH)[test] \
		&& ./$(VENV_SUBDIR)/bin/python -m pytest '

docs: COPY_PATH=docs/build
docs: cp-docs

$(PACKAGE_WHEEL): COPY_PATH=$(PACKAGE_WHEEL)
$(PACKAGE_WHEEL): cp-dist

# Generic target to copy paths out of the Docker image
cp-%: $(PACKAGE_DISTNAME).docker-stamp
	mkdir -p $$(dirname $(COPY_PATH))
	id=$$($(DOCKER) create $(PACKAGE_DOCKER_IMAGE)); \
	   $(DOCKER) cp $${id}:$(DOCKER_PATH)/$(COPY_PATH) $(COPY_PATH); \
	   $(DOCKER) rm -v $${id} >/dev/null

clean-docker:
	-$(DOCKER) rmi $(PACKAGE_DOCKER_IMAGE) $(NGINX_DOCKER_IMAGE)
	rm -f $(PACKAGE_DISTNAME).docker-stamp $(NGINX_DISTNAME).docker-stamp
clean: clean-docker

endif

clean: clean-pycache clean-docs clean-i18n
	rm -rf $(EGG_INFO)
	rm -rf $(BUILD_DIR)/dist
	rm -rf $(BUILD_DIR)/build
	test -z "$(VENV_SUBDIR)" || test "$(VENV_SUBDIR)" = '.' || rm -rf $(VENV_DIR)

clean-pycache:
	find $(SRC_DIR)/mediagoblin -type d -name '__pycache__' -exec rm -rf {} +
clean-docs:
	rm -rf $(DOCS_BUILD_DIR)
clean-i18n:
	find $(I18N_DIR) -type d -name 'LC_MESSAGES' -exec rm -rf {} +

distclean: clean
	rm -rf $(EXTLIB_JS)
	rm -f $(SRC_DIR)/config.guess $(SRC_DIR)/config.sub $(SRC_DIR)/configure
	rm -rf $(SRC_DIR)/aclocal.m4 $(SRC_DIR)/autom4te.cache/
	rm -f $(BUILD_DIR)/Makefile
	rm -rf \
		Dockerfile.nginx \
		mediagoblin/_version.py \
		docker-compose.build.yml \
		docker-compose.ecs.yml \
		docker-compose.lazyserver.yml \
		docker-compose.nginx.yml \
		docker-compose.yml \
		# end AC_CONFIG_FILES
	rm -f $(BUILD_DIR)/config.h $(BUILD_DIR)/config.log $(BUILD_DIR)/config.status


.PHONY: dist build \
	i18n extlib docs
